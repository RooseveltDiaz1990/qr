import { Component } from '@angular/core';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { Platform, ToastController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  constructor(
    private qrScanner: QRScanner,
    public platform: Platform,
    private toastCtrl: ToastController,
    private iab: InAppBrowser
  ) {
    this.platform.backButton.subscribeWithPriority(0, () => {
      document.getElementById('body').style.opacity = '1';
      document.getElementById('divUrl').style.display = 'none';
      document.getElementById('strUrl').innerText = '';
      const b = document.getElementById('btnOpen');
      b.classList.add('btn-disable');

      this.scanSub.unsubscribe();
    });
  }
  public flagUrl = false;
  public flagImg = false;
  public flagBtn = true;
  public url = '';

  scanSub: any;
  qrText: string;
  scanSubscription;

  startScanning() {

    this.flagImg = true;
    // Optionally request the permission early
    this.qrScanner
      .prepare()
      .then(async (status: QRScannerStatus) => {
        if (status.authorized) {
          this.qrScanner.show();

          // (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');

          document.getElementById('body').style.opacity = '0';
          document.getElementById('p').style.display = '';

          this.flagImg = true;
          this.scanSub = this.qrScanner.scan().subscribe(
            async (textFound: string) => {
              document.getElementById('body').style.opacity = '1';
              document.getElementById('p').style.display =
                'none';
              this.qrScanner.hide();
              this.flagImg = false;
              console.log(textFound);
              this.flagUrl = true;
              await this.validateUrl(textFound);
              this.scanSub.unsubscribe();
            },
            err => {
              this.flagImg = false;
              alert(JSON.stringify(err));
            }
          );
        } else if (status.denied) {
          this.flagImg = false;
          const toast = await this.toastCtrl.create({
            message: `Debes permitir el uso de la cama para continuar`,
            position: 'top',
            duration: 3000,
            closeButtonText: 'OK'
          });
          toast.present();
          this.startScanning();
        } else {
          this.flagImg = false;
          this.startScanning();
        }
      })
      .catch((e: any) => {
        console.log('Error is', e);
        this.flagImg = false;
      });
  }

  async validateUrl(url) {
    const pattern = /^(http|https)\:\/\/[a-z0-9\.-]+\.[a-z]{2,4}/gi;

    if (url.match(pattern)) {
      this.qrText = url;
      this.flagUrl = true;
      document.getElementById('divUrl').style.display = 'block';
      document.getElementById('strUrl').innerText = url;
      this.open();
    } else {
      const toast = await this.toastCtrl.create({
        message: 'Dirección url incorrecta',
        position: 'top',
        duration: 5000,
        closeButtonText: 'OK'
      });
      toast.present();
    }
  }

  closeApp() {
    navigator['app'].exitApp();
  }

  open() {
    const browser = this.iab.create(this.qrText, '_system');
    browser.close();
  }

}
